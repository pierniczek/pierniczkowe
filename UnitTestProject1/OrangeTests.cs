﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace UnitTestProject1
{
    [TestFixture]
    public class OrangeTests
    {
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
        }

        [Test, Order(2)]
        public void Logowanie()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
            driver.FindElement(By.XPath("//input[@id='txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.Id("txtPassword")).SendKeys("admin123");
            driver.FindElement(By.Id("btnLogin")).Click();
            IWebElement welcomeAdmin = driver.FindElement(By.Id("welcome"));

            Actions mouseMove = new Actions(driver);
            mouseMove.MoveToElement(welcomeAdmin);
            Assert.IsNotNull(welcomeAdmin);
        }

        [Test, Order (1)]
        public void DashboardClick()
        {

        }

        //komentarz na potrzeby zmian
        [TearDown]
        public void TearDown()
        {
            Thread.Sleep(2000);
            driver.Close();
        }
    }
}
